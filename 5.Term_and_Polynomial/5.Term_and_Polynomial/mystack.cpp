#include "mystack.h"

///////////////////////////////////////
//			������� �����			 //
///////////////////////////////////////

template <class DATA, class NODE>
node<DATA, NODE>::node() {
	this->info = DATA();
	this->next = nullptr;
}

template <class DATA, class NODE>
node<DATA, NODE>::node(DATA date, node* next) {
	this->info = date;
	this->next = next;
}

///////////////////////////////////////
//			  ��� �����				 //
///////////////////////////////////////

template <class DATA>
int mystack<DATA>::Size() {
	return this->size;
}

template <class DATA>
bool mystack<DATA>::empty() {
	if (this->top == nullptr)
		return true;
	else return false;
}

template <class DATA>
void mystack<DATA>::push(DATA data) {
	if (this->empty()) {
		this->size=1;
		this->top = new Node(data, nullptr);
	}
	else {
		this->size++;
		Node* c = new Node(data, this->top);
		this->top = c;
	}
}

template <class DATA>
DATA mystack<DATA>::operator[](int g){
	Node* buf=this->top;
	for(int i=0;i<g;i++){
		buf = buf->next;
	}
	return buf->info;
}

template <class DATA>
void mystack<DATA>::pop() {
	if (!this->empty()) {
		Node* buf = this->top->next;
		delete this->top;
		this->top = buf;
		this->size--;
	}
}

template <class DATA>
mystack<DATA>::mystack() {
	this->top = nullptr;
}

template <class DATA>
mystack<DATA>::mystack(mystack<DATA>& s) {
	Node* buf = s.top;
	while (buf) {
		if (this->empty()) {
			this->size=1;
			this->top = new Node(buf->info, nullptr);
			buf = buf->next;
		}
		else {
			this->size++;
			Node* c = new Node(buf->info, this->top);
			this->top = c;
			buf = buf->next;
		}
	}
}

template <class DATA>
mystack<DATA>::~mystack() {
	while (this->top) {
		this->pop();
	}
}

template <class DATA>
DATA mystack<DATA>::top_info() {
	return this->top->info;
}

template <class DATA>
void mystack<DATA>::print() {
	Node* a = this->top;
	while (a) {
		cout << a->info;
		a = a->next;
		if(a) cout << "*";
	}
	cout << endl;
}

/*template <class DATA>
ostream& operator<<(ostream& out, mystack<DATA>& s){
	Node* a = s.top;
	while (a) {
		out << a->info;
		a = a->next;
		if(a) out << "*";
	}
	out << endl;
	return out;
}*/

template <class DATA>
mystack<DATA>& mystack<DATA>::operator=(mystack<DATA>& s) {
	this->~mystack();
	Node* buf = s.top;
	while (buf) {
		if (this->empty()) {
			this->size=1;
			this->top = new Node(buf->info, nullptr);
			buf = buf->next;
		}
		else {
			this->size++;
			Node* c = new Node(buf->info, this->top);
			this->top = c;
			buf = buf->next;
		}
	}
}

template <class DATA>
void Multipliers(int n,mystack<DATA>& s) {
	int b = n;
	if (n != -1 && n != 0 && n != 1) {
		int i = 2;
		while (n != 1 && n!=-1 ) {
			while ((n%i == 0)) {
				n /= i;
				s.push(i);
			}
			i++;
		}
		if (n != 1 )
		s.push(n);
	}
	//s.print();
	cout << s;
	mystack<DATA> h = s;
	cout << b << " = ";
	h.print();
}