#include "mystr.h"

//////////////////////////////////////////////
//			������������ ������				//
//////////////////////////////////////////////

mystr::mystr(const mystr& str)
{
	if(this->str != nullptr)
		delete[] this->str;
	int len = strlen(str.str);
	this->str = new char[len+1];
	strcpy_s(this->str,len+1,str.str);
	this->str[len] = '\0';// ��������� ������ ������
}

mystr::mystr(char* str)
{
	int len = strlen(str);
	this->str = new char[len+1];
	strcpy_s(this->str,len+1,str);
	this->str[len] = '\0';// ��������� ������ ������
}

mystr::mystr()
{
	str=nullptr;
}

//////////////////////////////////////////////
//			���������� ������������			//
//////////////////////////////////////////////

const mystr& mystr::operator=(const mystr& str){
	if(this->str != nullptr)
		delete[] this->str;
	int len = strlen(str.str);
	this->str = new char[len+1];
	strcpy_s(this->str,len+1,str.str);
	this->str[len] = '\0';// ��������� ������ ������
	return str;
}

char& mystr::operator[](int i){
	return this->str[i];
}

istream& operator>> (istream &in, mystr &str){
	if(str.str != nullptr)
		delete[] str.str;

	str.str = nullptr;
	char *buf = nullptr;// �����

	char c = getchar();
	if(c != '\n')
		cin.putback(c);
	int size = 0;
	do {
		do{
			c = getchar();// ��������� ������
		}while((c==' ' || c == '\n') && size==0);
		if ((c == ' ' || c == '\n') && size!=0){// ��������
			if(c=='\n')
				break;
			else{
				do{
					c = getchar();
				}while(c!='\n');
				break;
			}
		}
		size += 1;
		if(size == 1){// ��������� ������ ��� ������
			str.str = new char[size+1];
			str.str[0] = c;
			str.str[size] = '\0';
		}
		else {// ������������ ������ ��� ������
			buf = new char[size+1];
			strcpy_s(buf,size+1,str.str);
			buf[size-1] = c;
			buf[size] = '\0';// ��������� ������ ������
			delete[] str.str;
			str.str = new char[size+1];// ������������ ������ ��� ������
			strcpy_s(str.str,size+1,buf);
			str.str[size] = '\0';
			delete[] buf;
		}
		
	} while (true);
	return in;
}


ostream& operator<< (ostream &out, mystr& str){
	out << str.str;
	return out;
}


ifstream& operator>> (ifstream &in, mystr &str){
	int size = 0;
	bool b = true;
	char c;
	do {
		in.get(c);
		if((c == ' ' || c == '\n' || c == in.eof()) && size != 0){
			b = false;
			if(c == '\n')
				size+=1;
		}
		if((c == ' ' || c == '\n') && size == 0){
			continue;
		}
		if(c != ' ' || c != '\n')
			size += 1;
	} while (b);
	in.seekg(-size, ios::cur);// ��������� ������� ������
	str.str = new char[size+1];
	in >> str.str;
	return in;
}

ofstream& operator<< (ofstream &out, mystr &str){
	out << str.str;
	return out;
}

//////////////////////////////////////////////
//			���������� ������				//
//////////////////////////////////////////////

mystr::~mystr()
{
	delete[] str;
}