﻿#include <iostream>
#include "polynomial.h"
#include "term.h"

using namespace std;

void laba(int laba) {// Главная надпись
	cout << setw(20);

	for (int i = 0; i < 50; i++) {

		cout << "*";

	}
	cout << endl << setw(21) << "**" << setw(34) << "Лабораторная работа №" << laba << setw(13) << "**" << endl;
	cout << setw(21) << "**" << setw(37) << "Полином состоящий из термов" << setw(11) << "**" << endl;
	cout << setw(20);

	for (int i = 0; i < 50; i++) {
		cout << "*";
	}

	cout << endl;
}

void menu() {
	while (true) {
		laba(5);
		cout << "1 - Ввод полинома\n";
		cout << "2 - Умножение полиномов\n";
		cout << "3 - Сложение полиномов\n";
		cout << "e - Выход\n";
		char n;
		cout << "Ваш выбор: ";
		cin >> n;
		switch (n)
		{
		case '1': {
			system("cls");
			cout << "Пример ввода полинома: 5x^5+x^2-7-(-6x^2)+7x-28" << endl;
			poly a;
			cout << "Введите полином: ";
				cin >> a;
			cout << "Результат: " << a << endl;
			break;
		}
		case '2': {
			system("cls");
			cout << "2 - Умножение полиномов\n";
			cout << "Пример ввода полинома: 5x^5+x^2-7-(-6x^2)+7x-28" << endl;
			poly a,b;
			cout << "Введите 1-й полином: ";
				cin >> a;
			cout << "Введите 2-й полином: ";
				cin >> b;
			cout << "Результат: " << a*b << endl;
			break;
		}
		case '3': {
			system("cls");
			cout << "3 - Сложение полиномов\n";
			cout << "Пример ввода полинома: 5x^5+x^2-7-(-6x^2)+7x-28" << endl;
			poly a,b;
			cout << "Введите 1-й полином: ";
				cin >> a;
			cout << "Введите 2-й полином: ";
				cin >> b;
			cout << "Результат: " << a+b << endl;
			break;
		}
		default:
			exit(0);
			break;
		}
	}
}

int main(){
	setlocale(LC_ALL, "ru");
	while(true){
		menu();
	}
	//poly x("x-1"),y("x^2+1+x");
	//cout << x*2 << endl;
	system("pause");
	return 0;
}