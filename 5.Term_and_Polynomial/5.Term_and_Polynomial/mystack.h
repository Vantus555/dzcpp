#ifndef Mystck_H
#define Mystck_H

#include <iostream>
#include <iomanip>
using namespace std;

///////////////////////////////////////
//			������� �����			 //
///////////////////////////////////////

template <class DATA, class NODE>
class node
{
	node* next;
	DATA info; // ���������� ����
	friend NODE;
	node();
	node(DATA, node*);
	friend ostream& operator<<(ostream& out, NODE& s);
};
///////////////////////////////////////
//			  ��� ����				 //
///////////////////////////////////////


template <class DATA> 
class mystack
{
private:
	typedef node<DATA, mystack<DATA>> Node;
	Node* top;
	int size;
public:
	mystack();
	mystack(mystack&);
	~mystack();
	int Size();
	void print();
	void push(DATA data);
	void pop();
	bool empty();
	DATA top_info();
	mystack& operator=(mystack&);
	DATA operator[](int i);
	friend ostream& operator<<(ostream& out, mystack& s) {
		Node* a = s.top;
		while (a) {
			out << a->info;
			a = a->next;
			if(a) {
				if(!a->info)
					out << "+";
			}
		}
		return out;
	}
};




#endif;

