#ifndef Polynomial_H
#define Polynomial_H

#include <iostream>
#include "mystr.h"
#include "term.h"
#include "mystack.cpp"

using namespace std;

class poly
{
private:
	int size;
	mystr str;
	term* pt;
	mystack<term> terms;
public:
	friend istream& operator>>(istream &in, poly &p);
	friend ostream& operator<<(ostream &out, poly &p);
	term operator[](int i);
	void operator=(poly& p);
	poly operator+(poly& p);
	poly operator-(poly& p);
	poly operator*(poly& p);
	poly operator*(term& t);
	poly operator*(int a);
	poly operator*(double a);
	void operator=(mystr cstr);
	void reduce();
	void sortmass();
	void sort();
	poly(mystr cstr);
	poly(poly& p);
	poly(term& t);
	poly();
	~poly(void);
};

#endif