#include "term.h"

istream& operator>>(istream& is, term& t){
	char c = getchar();
	if(c!='\n')
		cin.putback(c);
	int index=0;
	bool minus = false;
	bool one = false;
	do{
		c = getchar();
		if(c=='-') minus = true;
		if(c >= '0' && c <= '9'){
			one = true;
			index = index*10 + (int(c) - '0');
			continue;
		}
	}while(c!='\n' && c!='x' && c!='X');
	
	if(minus){
		if(one)
			t.index = -index;
		else t.index = -1;
	}
	else{
		if(one)
			t.index = index;
		else t.index = 1;
	}

	int degree=0;
	minus = false;
	one = false;
	if(c=='x' || c=='X'){
		do{
			c = getchar();
			if(c=='-') minus = true;
			if(c >= '0' && c <= '9'){
				one = true;
				degree = degree*10 + (int(c) - '0');
				continue;
			}
		}while(c!='\n' && c!=' ');
	}

	if(minus){
		if(one)
			t.degree = -degree;
		else t.degree = 1;
	}
	else{
		if(one)
			t.degree = degree;
		else t.degree = 1;
	}
	return is;
}

ostream& operator<<(ostream& of, term& t){
	if(t.index < 0 && t.degree==0){
		of << t.index;
	}
	else if(t.index == 0)
		of << 0 << endl;
	else if(t.degree==0 && t.index > 0){
		of << t.index;
	}
	else {
		if(t.index == -1 && t.degree != 1)
			of << "-x^" << t.degree;
		else if(t.index == 1 && t.degree != 1)
			of << "x^" << t.degree;
		else if(t.index == -1 && t.degree == 1)
			of << "-x";
		else if(t.index == 1 && t.degree == 1)
			of << "x";
		else{
			if(t.degree == 1){
				of << t.index << "x";
			}
			else{
				of << t.index << "x^" << t.degree;
			}
		}
	}
	return of;
}

term term::operator+(term& t){
	term buf;
	if(this->degree!=t.degree){
		buf.index = 0;
		buf.degree = 0;
	}
	else {
		buf.index = this->index + t.index;
		buf.degree = this->degree;
	}
	return buf;
}

term term::operator*(term& t){
	term buf(this->index*t.index,this->degree+t.degree);
	return buf;
}

bool term::operator!(){
	if(this->index > 0)
		return true;
	else return false;
}


bool term::operator<(term& t){
	if(this->degree < t.degree)
		return true;
	else return false;
}

bool term::operator>(term& t){
	if(this->degree > t.degree)
		return true;
	else return false;
}

bool term::operator==(term& t){
	if(this->degree == t.degree)
		return true;
	else return false;
}

bool term::operator!=(term& t){
	if(this->degree != t.degree)
		return true;
	else return false;
}

int term::Index(){
	return this->index;
}
int term::Degree(){
	return this->degree;
}

term::term(int index,int degree){
	this->index = index;
	this->degree = degree;
}


term::~term(void)
{
}
