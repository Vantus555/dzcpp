#ifndef Term_H
#define Term_H

#include <iostream>

using namespace std;

class term
{
private:
	int index;
	int degree;
public:
	int Index();
	int Degree();
	term operator+(term& t);
	term operator*(term& t);
	friend istream& operator>>(istream& is, term& t);
	friend ostream& operator<<(ostream& of, term& t);
	bool operator!();
	bool operator<(term& t);
	bool operator>(term& t);
	bool operator==(term& t);
	bool operator!=(term& t);

	term(int index = 0,int degree = 0);
	~term(void);
};


#endif