#include "polynomial.h"

void poly::reduce(){
	int res1=0;
	int res2=0;
	int minus = 1;
	bool one = false;
	bool x = false;
	bool end=false;
	for(int i=0;i < strlen(this->str);i++){
		if(str[i]=='-' && !x) {
			minus *= -1;
			continue;
		}
		if(str[i] >= '0' && str[i] <= '9' && !x){
			one = true;
			res1 = res1*10 + (int(str[i]) - '0');
			if(((str[i+1] < '0' && str[i+1] > '9') && str[i+1]!='x') || str[i+1]=='\0' || str[i+1]=='-' || str[i+1]=='+' || str[i+1]==')'){
				term t(minus*res1,0);
				this->terms.push(t);
				minus = 1;
				one = false;
				res1=res2=0;
				continue;
			}
			else continue;
		}
		
		if((str[i] == 'x' || str[i] == 'X') && !x){
			if(!one){
				res1=1;
			}
			if(minus == -1){
				res1*=minus;
			}
			minus = 1;
			one = false;
			x=true;
			if(str[i+1]=='\0'){
				term t(res1,1);
				this->terms.push(t);
			}
			continue;
		}
		if(x){
			if(str[i]=='^'){
				i++;
				for(int j=i;j <= strlen(this->str);j++){
					if(str[j]=='-') {
						minus *= -1;
						continue;
					}
					if(str[j] >= '0' && str[j] <= '9'){
						one = true;
						res2 = res2*10 + (int(str[i]) - '0');
						if(str[j+1] != '-' && str[j+1] != '\0')
							i++;
					}
					if(!one)
						res2=1;
					if(str[j+1] == '-' || str[j+1] == '+' || str[j+1] == '\0'){	
						term t(res1,minus*res2);
						this->terms.push(t);
						if(str[j+1] == '-' || str[j+1] == '+'){
							x=false;
							res1=res2=0;
							minus = 1;
							one = false;
							break;
						}
						if(str[j+1] == '\0'){
							end=true;
							break;
						}
					}
				}
			}
			else{
				i--;
				term t(res1,1);
				this->terms.push(t);
				x=false;
				res1=res2=0;
				minus = 1;
				one = false;
				continue;
			}
		}
		if((str[i] == '-' || str[i] == '+' || str[i] == '\0') && res1!=0 && !end){
			term t(minus*res1,0);
			this->terms.push(t);
			x=false;
			res1=res2=0;
			minus = false;
			one = false;
			continue;
		}
	}
}

void poly::sort(){
	for(int i = 0;i<this->size;i++){
		for(int j = 0;j<this->size-1;j++){
			if(pt[j] < pt[j+1]){
				term t = pt[j];
				pt[j] = pt[j+1];
				pt[j+1] = t;
			}
		}
	}
}

void poly::sortmass(){
	this->size = this->terms.Size();
	this->pt = new term[this->size];
	for(int i = 0;i<this->size;i++){
		pt[i] = this->terms[i];
	}
	this->sort();

	int count=0;
	this->terms.~mystack();
	term tt=this->pt[0];
	for(int i = 1;i<this->size+1;i++){
		if(tt == pt[i]){
			tt = tt + pt[i];
			if(i == this->size){
				terms.push(tt);
			}/**/
		}
		else{
			terms.push(tt);
			tt = pt[i];
		}
	}
	this->size = this->terms.Size();
	delete[] this->pt;
	this->pt = new term[this->size];
	for(int i = 0;i<this->size;i++){
		pt[i] = this->terms[i];
	}
	this->terms.~mystack();
	this->sort();
}

term poly::operator[](int i){
	return this->terms[i];
}

istream& operator>>(istream &in, poly &p){
	in >> p.str;
	p.reduce();
	p.sortmass();
	return in;
}

ostream& operator<<(ostream &out, poly &p){
	for(int i = 0;i<p.size;i++){
		if(i==0 && p.pt[i].Index()>0)
			out << p.pt[i];
		else if(i!=0 && p.pt[i].Index()>0)
			out << "+" << p.pt[i];
		else if(p.pt[i].Index()!=0)
			out << p.pt[i];
	}
	return out;
}

poly::poly(mystr cstr)
{
	this->str = cstr;
	this->pt = nullptr;
	if(cstr){
		this->reduce();
		this->sortmass();
	}
}


poly::poly(poly& p){
	this->size = p.size;
	this->pt = new term[p.size];
	for(int i=0; i<this->size; i++){
		this->pt[i] = p.pt[i];
	}
}


void poly::operator=(poly& p){
	delete[] this->pt;
	this->size = p.size;
	this->pt = new term[p.size];
	for(int i=0; i<this->size; i++){
		this->pt[i] = p.pt[i];
	}
}

void poly::operator=(mystr cstr){
	delete[] this->pt;
	this->pt = nullptr;
	this->str = cstr;
	if(cstr){
		this->reduce();
		this->sortmass();
	}
}

poly poly::operator*(poly& p){
	poly res;
	res.size = (this->size) * (p.size);
	for(int i=0;i<this->size;i++){
		for(int j=0;j<p.size;j++){
			term t = this->pt[i]*p.pt[j];
			res.terms.push(t);
		}
	}
	res.sortmass();
	return res;
}

poly poly::operator+(poly& p){
	poly res;
	res.size = this->size + p.size;
	for(int i=0;i<this->size;i++){
		res.terms.push(this->pt[i]);
	}
	for(int j=0;j<p.size;j++){
		res.terms.push(p.pt[j]);
	}
	res.sortmass();
	return res;
}


poly poly::operator*(term& t){
	poly p(t);
	return *this*p;
}

poly poly::operator*(int a){
	term t(a);
	poly y(t);
	return (*this)*y;
}

poly poly::operator*(double a){
	term t(a);
	poly y(t);
	return (*this)*y;
}

poly poly::operator-(poly& p){
	return *this+p*(-1);
}

poly::poly(){
	this->size=1;
	term t(0,0);
	terms.push(t);
		this->sortmass();
}

poly::poly(term& t){
	terms.push(t);
		this->sortmass();
}

poly::~poly()
{
	delete[] this->pt;
}
