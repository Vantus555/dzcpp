#pragma once
#include <fstream>
#include <iostream>
#include <iomanip>
#include "mystr.h"

using namespace std;
class planet
{
private:
	mystr name;
	double diameter;
	bool life;
	int satellite;
	static unsigned int counter;
public:
	planet();
	planet(mystr,double,bool,int);
	planet(const planet& p);
	////////////////////////////////////////////////////
	planet& operator=(planet& p);
	bool operator>(planet);
	bool operator<(planet);
	bool operator==(char*);
	friend ofstream& operator<<(ofstream&, planet&);
	friend ifstream& operator>>(ifstream&, planet&);
	friend ostream& operator<<(ostream&, planet&);
	friend istream& operator>>(istream&, planet&);
	////////////////////////////////////////////////////
	double getd();
	bool getlife();
	int getsete();
	void print();
	friend void edit_mode();
	~planet(void);
};

//////////////////////////////////////////////
//		���������������� �������			//
//////////////////////////////////////////////

	void laba(int num_laba);
	void write_to_file();
	void read_file();
	void add_planet();
	void del();
	void del_planet();
	void search_planet();
	void edit_mode();
	void sort_planet();
	void print_arr();
	
