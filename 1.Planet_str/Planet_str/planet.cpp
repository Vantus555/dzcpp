#include "planet.h"

const char* filename = "Planet_Base.txt";

char* add_name();

unsigned int planet::counter = 0;

static planet *arr = nullptr;
static int size_mass = 0;

//////////////////////////////////////////////
//			������������ ������				//
//////////////////////////////////////////////

planet::planet()
{
	counter++;// ������� ��������
	cout << "�������� ������� �" << counter << endl;
}


planet::planet(mystr Name,double D,bool Life,int sate)
{
	counter++;// ������� ��������
	cout << "�������� ������� �" << counter << endl;
	this->name = Name;
	this->diameter = D;
	this->life = Life;
	this->satellite = sate;
}

planet::planet(const planet& p) {
	counter++;// ������� ��������
	cout << "�������� ������� �" << counter << endl;
	this->name = p.name;
	this->diameter = p.diameter;
	this->life = p.life;
	this->satellite = p.satellite;
}

//////////////////////////////////////////////
//			���������� ������������			//
//////////////////////////////////////////////

planet& planet::operator=(planet& p) {
	this->name = p.name;
	this->diameter = p.diameter;
	this->life = p.life;
	this->satellite = p.satellite;
	return p;
}

bool planet::operator>(planet p) {
	return (strcmp(this->name, p.name) > 0) ? true : false;
}

bool planet::operator<(planet p) {
	return (strcmp(this->name, p.name) < 0) ? true : false;
}

bool planet::operator==(char* Name) {
	return (strcmp(this->name, Name) == 0) ? true : false;
}

ofstream& operator<<(ofstream& ofs, planet& p) {
	ofs << p.name << " " << p.diameter << " " << p.life << " " << p.satellite << endl;
	return ofs;
}

ifstream& operator>>(ifstream& ifs, planet& p) {
	ifs >> p.name >> p.diameter >> p.life >> p.satellite;
	return ifs;
}

ostream& operator<<(ostream& ost, planet& p) {
	ost << "\t"  << setiosflags(ios::left) << setw(32) << p.name << p.diameter << "\t\t" << (p.life ?  "true" :  "false") << "\t\t" << p.satellite << endl;
	return ost;
}

istream& operator>>(istream& ist, planet& p) {
	bool b;
	do {
		cout << "������� �������� �������: ";
		cin >> p.name;
		for(int i = 0;i < size_mass;i++){
			if(arr[i] == p.name){
				b = true;
				cout << "������� � ����� ������ ��� ����������!\n";
				break;
			}
			else b = false;
		}
	}while(b);
	do{
		cout << "������� ������� �������: ";
		ist >> p.diameter;
	}while(p.diameter < 0);
	int n;
	do{
		cout << "������� ��������� ���� �������(0/1): ";
		ist >> n;
	}while(n != 1 && n != 0);
	p.life = n;
	do{
		cout << "������� ���-�� ��������� �������: ";
		ist >> p.satellite;
	}while(p.satellite < 0);
	return ist;
}
//////////////////////////////////////////////
//			���������� ������				//
//////////////////////////////////////////////

planet::~planet(void)
{
	cout << "�������� ������� �" << counter << endl;
	counter--;// ������� ��������
}

//////////////////////////////////////////////////
//		������� ��� ������� � ����� ������		//
//////////////////////////////////////////////////

void planet::print(){ // ����� ����� �������
	cout << "�������: " << this->name << endl;
	cout << "������� �������: " << this->diameter << endl;
	cout << "����� �������: " << (this->life ? "true" : "false") << endl;
	cout << "���-�� ���������: " << this->satellite << endl << endl;
}

double planet::getd() {
	return this->diameter;
}
bool planet::getlife() {
	return this->life;
}
int planet::getsete() {
	return this->satellite;
}


//////////////////////////////////////////////
//		���������������� �������			//
//////////////////////////////////////////////



void del() {// ������������ ������ ������� ������
	delete[] arr;
}

void laba(int laba) {// ������� �������
	cout << setw(30);

	for (int i = 0; i < 30; i++) {

		cout << "*";

	}
	cout << endl << setw(31) << "**" << setw(23) << "������������ ������ �" << laba << setw(4) << "**" << endl;
	cout << setw(31) << "**" << setw(22) << "���� ������ ������" << setw(6) << "**" << endl;
	cout << setw(30);

	for (int i = 0; i < 30; i++) {
		cout << "*";
	}

	cout << endl;
}

// ������� � ���� �������
void write_to_file() {
	ofstream fout(filename);
	for (int i = 0; i < size_mass; i++) 
		fout << arr[i];
	fout.close();
}

void read_file() {
		size_mass = 0;// ������� ������
		char c;
		ifstream fin(filename);
		int b = 0;//������� ��������
		while (!fin.eof()) {// ������� �����(��������)
			fin.get(c);
			if(c!=' ' && c!= '\n')
				b += 1;
			if (c == '\n' && b!=0) {
				b = 0;
				size_mass += 1;
			}
		}
		if(c != '\n')
			size_mass += 1; // ���� ��� ���������� �������� ������
		fin.close();
		if(size_mass == 0 || size_mass == 1)
			cout << "���� ����!!!" << endl;
		else{
			arr = new planet[size_mass];// ��������� ������ ��� ������
			ifstream fin(filename);
			for (int i = 0; i < size_mass; i++) {
				fin >> arr[i];// ��������� ����� ������(������ �������)
			}
			fin.close();
		}
}

void print_arr(){
		cout  << "  " << "�" << "\t" << "���" << "\t\t\t\t" << "�������(��)" << "\t" << "�����" << "\t\t" << "��������" << endl;
		for (int i = 0; i < size_mass; i++) {
			cout << endl;
			cout << "  " << i+1;
			cout << arr[i];
		}
		cout << resetiosflags(ios::left) << endl;
}

void add_planet() {// ���������� �������
	planet* buf = new planet[size_mass];
	for(int i=0;i<size_mass;i++){
		buf[i]=arr[i];
	}
	delete[] arr;
	arr = new planet[size_mass+1];
	for(int i=0;i<size_mass;i++){
		arr[i]=buf[i];
	}
	cin >> arr[size_mass];
	delete[] buf;
	size_mass+=1;
}

void del_planet() {// �������� �������
	print_arr();

	char c;
	cout << "1 - �������� �� ������" << endl;
	cout << "2 - �������� �� �����" << endl;
	cout << "��� �����: ";
	cin >> c;

	switch (c)
	{
	case '1':
		{
			int num;
			do{
				cout << "������� ����� ������� ������� ������ �������: ";
				cin >> num;
			}while(num < 1 || num > size_mass);
			planet* buf = new planet[size_mass-1];
			for(int i=0;i<size_mass-1;i++){
				if(i < num - 1)
					buf[i]=arr[i];
				else
					buf[i]=arr[i+1];
			}
			delete[] arr;
			arr = new planet[size_mass-1];
			for(int i=0;i<size_mass-1;i++){
				arr[i]=buf[i];
			}
			delete[] buf;
			size_mass-=1;
		}
		break;
	case '2':{
			mystr str;
			bool b = true;
			do{
			cout << "������� ��� ������� ������� ������ �������: ";
			cin >> str;
				for(int i=0;i<size_mass;i++){
					if(arr[i]==str) {
						b = false;
						break;
					}
				}
				if(b)
				cout << "����� ������� �� ����������!\n";
			}while(b);

			planet* buf = new planet[size_mass-1];
			for(int i=0;i<size_mass-1;i++){
				if(!(arr[i]==str) && !b)
					{
						buf[i]=arr[i];
						b == true;
				}
				else
					buf[i]=arr[i+1];
			}
			delete[] arr;
			arr = new planet[size_mass-1];
			for(int i=0;i<size_mass-1;i++){
				arr[i]=buf[i];
			}
			delete[] buf;
			size_mass-=1;
		}
		break;
	default:
		break;
	}

	
}

void search_planet() {// ����� �������
	cout << "������� �������� �������: ";
	mystr Name;
	cin >> Name;
	bool b = true;
	for (int i = 0; i < size_mass; i++) {// ����� ������� � �����
		if (arr[i] == Name) {//strcmp(arr[i].getname(),Name) == 0
			cout << "\n������� �" << i+1 << endl;
			arr[i].print();
			b = false;
			break;
		}
	}
	if (b) {// ���� ��� �������
			cout << "����� ������� ������������!" << endl << endl;
	}
}

void edit_mode() {// ����� ��������������
	print_arr();
	char c;
	int num;
	mystr str;
	bool b = true;
	cout << "1 - �������� �� ������" << endl;
	cout << "2 - �������� �� �����" << endl;
	cout << "��� �����: ";
	cin >> c;

	switch (c)
	{
	case '1':
		{
			do{
				cout << "������� ����� ������� ������� ������ ���������������: ";
				cin >> num;
			}while(num > size_mass || num < 1);
		}
		getchar();
		break;
	case '2':{
			do{
			cout << "������� ��� ������� ������� ������ �������: ";
			cin >> str;
				for(int i=0;i<size_mass;i++){
					if(arr[i]==str) {
						b = false;
						num = i+1;
						break;
					}
				}
				if(b)
				cout << "����� ������� �� ����������!\n";
			}while(b);
		}
		break;
	default:
		break;
	}

	bool a = true;
	while(a){
		system("cls");
		cout << "�������������� �������: " << endl;
		arr[num-1].print();
		cout << "1 - �������������� �����" << endl;
		cout << "2 - �������������� ��������" << endl;
		cout << "3 - �������������� �����" << endl;
		cout << "4 - �������������� ���������" << endl;
		cout << "e - �����" << endl;
		cout << "��� �����: ";
		c = getchar();

		switch (c)
		{
		case '1':
			cout << "������� �������� �������: ";
			cin >> arr[num-1].name;
			break;
		case '2':
			do{
				cout << "������� ������� �������: ";
				cin >> arr[num-1].diameter;
			}while(arr[num-1].diameter < 0);
			break;
		case '3':
			int n;
			do{
				cout << "������� ����� �������(0/1): ";
				cin >> n;
			}while(n != 1 && n != 0);
			arr[num-1].life = n;
			break;
		case '4':
			do{
				cout << "������� ���-�� ��������� �������: ";
				cin >> arr[num-1].satellite;
			}while(arr[num-1].satellite < 0);
			break;
		default:
			a=false;
			break;
		}
	}
	
}

void sort_planet() {// ���������� �������
	int n, c;
	cout << "1 - ���������� �� �����\n";
	cout << "2 - ���������� �� �������� �������\n";
	cout << "3 - ���������� �� ����� �������\n";
	cout << "4 - ���������� �� ���-�� ���������\n";
	cout << "��� �����: ";
	cin >> n;
	cout << "\n1 - �� �����������; 2 - �� ��������;\n";
	cout << "��� �����: ";
	cin >> c;
	switch (n)
	{
	case 1:// 1 - ���������� �� �����
	{
		for (int i = 0; i < size_mass; i++) {
			for (int j = 0; j < size_mass - 1; j++) {
				if (c == 1) {
					if (arr[j] > arr[j + 1]) {//strcmp(arr[j].getname(), arr[j + 1].getname()) < 0
						planet buf = arr[j];
						arr[j]=arr[j + 1];
						arr[j + 1] = buf;
					}
				}
				if (c == 2) {
					if (arr[j] < arr[j + 1]) {//strcmp(arr[j].getname(), arr[j + 1].getname()) > 0
						planet buf = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = buf;
					}
				}
			}
		}
		cout << "���������� �� ����� ���������!\n\n";
	}
	break;
	case 2:// 2 - ���������� �� �������� �������
	{
		if (c == 1) {
			for (int i = 0; i < size_mass; i++) {
				for (int j = 0; j < size_mass - 1; j++) {
					if (arr[j].getd() < arr[j + 1].getd()) {
						planet buf = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = buf;
					}
				}
			}
		}
		if (c == 2) {
			for (int i = 0; i < size_mass; i++) {
				for (int j = 0; j < size_mass - 1; j++) {
					if (arr[j].getd() > arr[j + 1].getd()) {
						planet buf = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = buf;
					}
				}
			}
		}
	}
	cout << "���������� �� �������� ������� ���������!\n\n";
	break;
	case 3: {// 3 - ���������� �� ����� �������
		if (c == 1) {
			for (int i = 0; i < size_mass; i++) {
				for (int j = 0; j < size_mass - 1; j++) {
					if (arr[j].getlife() < arr[j + 1].getlife()) {
						planet buf = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = buf;
					}
				}
			}
		}
		if (c == 2) {
			for (int i = 0; i < size_mass; i++) {
				for (int j = 0; j < size_mass - 1; j++) {
					if (arr[j].getlife() > arr[j + 1].getlife()) {
						planet buf = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = buf;
					}
				}
			}
		}
		cout << "���������� �� ����� ������� ���������!\n";
	}
		break;
	case 4:// 4 - ���������� �� ���-�� ���������
		if (c == 1) {
			for (int i = 0; i < size_mass; i++) {
				for (int j = 0; j < size_mass - 1; j++) {
					if (arr[j].getsete() < arr[j + 1].getsete()) {
						planet buf = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = buf;
					}
				}
			}
		}
		if (c == 2) {
			for (int i = 0; i < size_mass; i++) {
				for (int j = 0; j < size_mass - 1; j++) {
					if (arr[j].getsete() > arr[j + 1].getsete()) {
						planet buf = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = buf;
					}
				}
			}
		}
		cout << "���������� �� ���-�� ��������� ���������!\n";
		break;
	default:
		break;
	}
}