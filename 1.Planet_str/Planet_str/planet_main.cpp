﻿// C++_ifstream.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include "planet.h"

using namespace std;

void menu() {
	read_file();
	while (true) {
		laba(1);
		cout << "1 - Сохранить изменения в файл\n";
		cout << "2 - Добавление плвнеты\n";
		cout << "3 - Удаление планеты\n";
		cout << "4 - Поиск планеты\n";
		cout << "5 - Вывод базы на экран\n";
		cout << "6 - Редактирование БД\n";
		cout << "7 - Cортировка БД\n";
		cout << "e - Выход\n";
		char n;
		cout << "Ваш выбор: ";
		cin >> n;
		switch (n)
		{
		case '1': {// l - Сохранить изменения в файл
			system("cls");

			write_to_file();

			cout << "\nФайл успешно заполнен значениями сохранен!" << endl << endl;
		}
		break;
		case '2':// 2 - Добавление плвнеты(работает)
			system("cls");
			cout << setw(50) << "Добавление планеты" << endl;
			add_planet();
			cout << "Планета добавлена успешно!" << endl << endl;
			break;
		case '3':// 3 - Удаление планеты(работает)
			system("cls");
			cout << setw(60) << "Данные о плвнетах из базы данных" << endl << endl;
			del_planet();
			cout << "Планета успешно удалена!" << endl << endl;
			break;
		case '4':// 4 - Поиск планеты(работает)
			system("cls");
			search_planet();
			break;
		case '5':// 5 - Вывод базы на экран(работает)
			system("cls");
			cout << setw(60) << "Данные о плвнетах из базы данных" << endl << endl;
			print_arr();
			break;
		case '6':// 6 - Редактирование БД(работает)
			system("cls");
			cout << setw(60) << "Данные о плвнетах из базы данных" << endl << endl;
			
			edit_mode();
			cout << "Планета успешно отредактирована!" << endl << endl;
			break;
		case '7':// 7 - Cортировка БД;
			system("cls");
			sort_planet();
			break;
		default:
			cout << "Записать измененные данные в файл?";
			char n;
			cout << "Ваш выбор(y/n): ";
			cin >> n;
			if (n == 'y'){ 
				write_to_file();
				del();
				system("pause");
				exit(0);
			}
			else {
				del();
				system("pause");
				exit(0);
			}
			break;
		}
	}
}

int main()
{
	setlocale(LC_ALL,"ru");


	menu();
	del();

	system("pause");
	return 0;
}