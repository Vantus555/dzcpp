#pragma once

#include <fstream>
#include <iostream>

using namespace std;

class mystr
{
private:
	char* str;

public:
	mystr(const mystr&);
	mystr(char*);
	mystr(void);
	~mystr(void);
	const mystr& operator=(const mystr&);
	operator char*()const{return str;}
	friend istream& operator>>(istream &in, mystr &str);
	friend ostream& operator<<(ostream &out, mystr &str);
	friend ifstream& operator>>(ifstream &in, mystr &str);
	friend ofstream& operator<<(ofstream &out, mystr &str);
};
