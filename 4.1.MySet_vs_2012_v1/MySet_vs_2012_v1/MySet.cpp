#include "MySet.h"

MySet operator+(MySet & s1, MySet & s2)
{
	MySet s = s1;
	for (int i = 0; i < s2.size; i++) {
		bool b = true;
		for (int j = 0; j < s.size;j++) {
			if (strcmp(s2.pdata[i], s.pdata[j]) == 0) {
				b = false;
				break;
			}
		}
		if (b)
			s.add_element(s2.pdata[i]);
	}
	return s;
}

MySet operator-(MySet & s1, MySet & s2)
{
	MySet s = s1;
	for (int i = 0; i < s1.size; i++) {
		bool b = false;
		for (int j = 0; j < s2.size; j++) {
			if (strcmp(s2.pdata[j], s1.pdata[i]) == 0) {
				b = true;
				break;
			}
		}
		if (b) {
			while (s.find(s1.pdata[i])!=-1) {
				int g = s.find(s1.pdata[i]);
				s.delete_element(g);
			}
		}
	}
	return s;
}

MySet operator*(MySet & s1, MySet & s2){
	MySet s = s1;
	for (int i = 0; i < s1.size; i++) {
		bool b = true;
		for (int j = 0; j < s2.size; j++) {
			if (strcmp(s2.pdata[j], s1.pdata[i]) == 0) {
				b = false;
				break;
			}
		}
		if (b) {
			while (s.find(s1.pdata[i])!=-1) {
				int g = s.find(s1.pdata[i]);
				s.delete_element(g);
			}
		}
	}
	return s;
}

bool MySet::operator==(MySet & ss)
{
	bool b = false;
	if (this->size == ss.size) {
		for (int i = 0; i < this->size; i++) {
			for (int j = 0; j < ss.size; j++) {
				if (strcmp(ss.pdata[j], this->pdata[i]) == 0) {
					b = true;
					break;
				}
			}
		}
	}
	return b;
}

MySet MySet::operator+=(MySet & ss)
{
	MySet s = *this;
	for (int i = 0; i < this->size; i++) {
		bool b = true;
		for (int j = 0; j < s.size; j++) {
			if (strcmp(ss.pdata[i], s.pdata[j]) == 0) {
				b = false;
				break;
			}
		}
		if (b)
			s.add_element(ss.pdata[i]);
	}
	*this = s;
	return *this;
}

MySet MySet::operator-=(MySet & ss)
{
	MySet s = *this;
	for (int i = 0; i < this->size; i++) {
		bool b = false;
		for (int j = 0; j < ss.size; j++) {
			if (strcmp(ss.pdata[j], this->pdata[i]) == 0) {
				b = true;
				break;
			}
		}
		if (b) {
			while (s.find(this->pdata[i]) != -1) {
				int g = s.find(this->pdata[i]);
				s.delete_element(g);
			}
		}
	}
	*this = s;
	return *this;
}

MySet MySet::operator*=(MySet & ss)
{
	MySet s = *this;
	for (int i = 0; i < this->size; i++) {
		bool b = true;
		for (int j = 0; j < ss.size; j++) {
			if (strcmp(ss.pdata[j], this->pdata[i]) == 0) {
				b = false;
				break;
			}
		}
		if (b) {
			while (s.find(this->pdata[i]) != -1) {
				int g = s.find(this->pdata[i]);
				s.delete_element(g);
			}
		}
	}
	*this = s;
	return *this;
}
