#include "MyVector.h"
#include <string.h>
#include <iostream>

using namespace std;

void MyVector::resize()
{
	if (size == maxsize) {
		if (maxsize % 2 == 0)
			maxsize *= 1.5;
		else maxsize = maxsize * 1.5 + 1;

		char ** buf = new char*[maxsize];

		for (int i = 0; i < size;i++) {
			int len = strlen(this->pdata[i]);
			buf[i] = new char[len];
			strcpy_s(buf[i],len+1,this->pdata[i]);
		}
		delete[] this->pdata;
		this->pdata = buf;
		/*this->pdata = new char*[maxsize];
		for (int i = 0; i < size;i++) {
			int len = strlen(buf[i]);
			buf[i] = new char[len];
			strcpy_s(buf[i],len+1,this->pdata[i]);
		}*/
	}
	if ((size < (maxsize / 2)) && (maxsize> 5)) {
		char ** buf = new char*[maxsize / 1.5];
		int j = 0;
		for (int i = 0; i < size; i++) {
			int len = strlen(this->pdata[i]);
			buf[j] = new char[len];
			strcpy_s(buf[j], len+1, this->pdata[i]);
			j++;
		}
		maxsize /= 1.5;
		delete[] this->pdata;
		this->pdata = buf;
	}
}

MyVector::MyVector()
{
	this->pdata = nullptr;
	this->size = 0;
	this->maxsize = 5;
}


MyVector::MyVector(char * el, int maxsz)
{
	this->size = 0;
	this->maxsize = maxsz;
	this->pdata = new char*[maxsz];
	if (el) {
		int len = strlen(el);
		if (len != 0) {
			this->pdata[0] = new char[len];
			strcpy_s(this->pdata[0], len + 1, el);
			size += 1;
		}
	}
}

MyVector::MyVector(MyVector & v)
{
	int len = v.maxsize;
	this->pdata = new char*[len];
	for (int i = 0; i < v.size; i++) {
		int slen = strlen(v.pdata[i]);
		this->pdata[i] = new char[slen];
		strcpy_s(this->pdata[i], slen + 1, v.pdata[i]);
	}
	this->size = v.size;
	this->maxsize = v.maxsize;
}

MyVector::~MyVector()
{
	delete[] this->pdata;
}

void MyVector::add_element(char * el)
{
	int len = strlen(el);
	if (size == maxsize)
		resize();
	this->pdata[size] = new char[len];
	strcpy_s(this->pdata[size],len+1,el);
	this->size += 1;
}

bool MyVector::delete_element(int i)
{
	if (size != 0 && (i >= 0) && (i < this->size)) {
		//char* buf = new char;
		//strcpy_s(buf,2,"-");
		//char* buf2 = this->pdata[i];
		this->pdata[i] = nullptr;
		//delete[] buf2;
		//strcpy_s(this->pdata[i], 1, "1");
		sort();
		this->size -= 1;
		resize();
	}
	else return false;
}

void MyVector::sort()
{
	for (int i = 0; i < this->size-1; i++){
		if (this->pdata[i] == nullptr) {
			for (int j = i+1; j < size;j++) {
				if (this->pdata[j] != nullptr) {
					this->pdata[i] = this->pdata[j];
					this->pdata[j] = nullptr;
					break;
				}
			}
		}
	}
}

char* MyVector::operator[](int i)
{
	if (this->pdata[i] != nullptr)
		return this->pdata[i];
	else return "NULL";
}

int MyVector::find(char * el)
{
	bool b = true;
	for (int i = 0; i < this->size;i++) {
		if (strcmp(this->pdata[i], el) == 0) {
			return i;
			b = false;
			break;
		}
	}
	if (b)
		return -1;
}

MyVector& MyVector::operator=(MyVector & v)
{
	if (this->pdata != nullptr)
		delete[] this->pdata;
	int len = v.maxsize;
	this->pdata = new char*[len];
	for (int i = 0; i < v.size; i++) {
		int slen = strlen(v.pdata[i]);
		this->pdata[i] = new char[slen];
		strcpy_s(this->pdata[i], slen + 1, v.pdata[i]);
	}
	this->size = v.size;
	this->maxsize = v.maxsize;

	return v;
}

ostream& operator<<(ostream& out, MyVector & v)
{
	if (v.size != 0) {
		cout << "{ ";
		for (int i = 0; i < v.size; i++) {
			cout << v[i];
			if (i < v.size - 1)
				cout << ", ";
			else cout << " }" << endl;
		}
		return out;
	}
	else {
		out << "{ NULL }" << endl;
	}
}
