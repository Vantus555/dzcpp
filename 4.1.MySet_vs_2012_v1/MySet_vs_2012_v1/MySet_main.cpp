#include <iostream>
#include <iomanip>
#include "MyVector.h"
#include "MySet.h"

using namespace std;

void laba(double laba) {// ������� �������
	cout << setw(30);

	for (int i = 0; i < 50; i++) {

		cout << "*";

	}
	cout << endl << setw(31) << "**" << setw(34) << "������������ ������ �" << laba << setw(11) << "**" << endl;
	cout << setw(31) << "**" << setw(36) << "��������� ���� string" << setw(12) << "**" << endl;
	cout << setw(30);

	for (int i = 0; i < 50; i++) {
		cout << "*";
	}

	cout << endl;
}

int main() {
	setlocale(0,"ru");

	laba(4.1);

	MyVector v("Hello!");
	v.add_element("������!");
	v.add_element("������!");
	v.add_element("������!");
	v.add_element("������!");
	v.add_element("������!");
	cout << "������ v: " << endl << v << endl;
	v.add_element("������!");
	v.add_element("������!");
	v.add_element("������!");
	cout << "������ v: " << endl << v << endl;
	MyVector v1 = v;
	cout << "������ v1: " << endl << v1 << endl;

	///////////////////////////////////////////////

	MySet s("Yes"), s1, s2;
	s.add_element("������!");
	s.add_element("No");
	char* str = "Hello!";
	s.add_element(str);
	cout << "��������� s: " << s << endl;
	s1.add_element("Cat");
	s1.add_element("No");
	s1.add_element("������!");
	cout << "��������� s1: " << s1 << endl;
	s2 = s1 - s;
	cout << "��������� s2=s1-s: " << s2 << endl;
	cout << "��������� s1: " << s1 << endl;
	cout << "��������� s: " << s << endl;
	s2 = s - s1;
	cout << "��������� s2=s-s1: " << s2 << endl;
	cout << "��������� s1: " << s1 << endl;
	cout << "��������� s: " << s << endl;
	s2 = s1 + s;
	cout << "��������� s2=s1+s: " << s2 << endl;
	cout << "��������� s1: " << s1 << endl;
	cout << "��������� s: " << s << endl;
	s2 = s1 * s;
	cout << "��������� s2=s1*s: " << s2 << endl;
	cout << "��������� s1: " << s1 << endl;
	cout << "��������� s: " << s << endl;
	MySet s3 = s2;
	cout << "��������� s3=s2: " << s3 << endl;
	if (s3 == s2)
		cout << "��������� s3=s2\n";
	else
		cout << "��������� s3!=s2\n";
	if (s3 == s1)
		cout << "��������� s3=s1\n";
	else
		cout << "��������� s3!=s1\n";
	if (s1 == s3)
		cout << "��������� s1=s3\n";
	else
		cout << "��������� s1!=s3\n";

	system("pause");
	return 0;
}