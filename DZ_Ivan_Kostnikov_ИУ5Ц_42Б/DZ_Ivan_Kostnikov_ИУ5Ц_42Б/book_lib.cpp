#include "book_lib.h"


book_lib::book_lib(mystr FIO,mystr Name,int year,int count){
	this->FIO = FIO;
	this->Name = Name;
	this->year = year;
	this->count = count;
}


ofstream& operator<<(ofstream& os, book_lib& b){
	os << b.FIO << " " << b.Name << " " << b.year << " " << b.count;
	return os;
}
ifstream& operator>>(ifstream& is, book_lib& b){
	is >> b.FIO;
	is >> b.Name;
	is >> b.year;
	is >> b.count;
	return is;
}
ostream& operator<<(ostream& os, book_lib& b){
	os << "\t" << setiosflags(ios::left) << setw(32) << b.FIO;
	os << setw(32) << b.Name << " ";
	os << b.year << " ";
	os << "\t\t" << b.count << endl;
	return os;
}
istream& operator>>(istream& is, book_lib& b){
	cout << "������� ���: ";
	is >> b.FIO;
	cout << "������� �������� �����: ";
	is >> b.Name;
	cout << "������� ���: ";
	is >> b.year;
	cout << "������� ���������� �����������: ";
	is >> b.count;
	return is;
}

book_lib& book_lib::operator=(book_lib& b){
	this->FIO = b.FIO;
	this->Name = b.Name;
	this->year = b.year;
	this->count = b.count;
	return b;
}

book_lib::book_lib(const book_lib& b){
	this->FIO = b.FIO;
	this->Name = b.Name;
	this->year = b.year;
	this->count = b.count;
}

book_lib::book_lib()
{
}


book_lib::~book_lib()
{
}

bool book_lib::operator==(mystr& s){
	if(strcmp(this->Name, s) == 0)
		return true;
	else return false;
}

void read_lib(mystr s, vector<book_lib>& v){
	ifstream f(s);
	book_lib b;
	char c;
	f >> c;

	if(!f.eof() && c){
		f.seekg(0);
		while(!f.eof()){
			f >> b;
			v.push_back(b);
		}
	}

	f.close();
}

void write_lib(mystr s, vector<book_lib>& v){
	ofstream f(s);
	
	for(int i = 0; i < v.size();i++){
		if(i==0)
			f << v[i];
		else {
			f << endl;
			f << v[i];
		}
	}

	f.close();
}

void laba(int laba) {// ������� �������
	cout << setw(30);

	for (int i = 0; i < 30; i++) {

		cout << "*";

	}
	cout << endl << setw(31) << "**" << setw(22) << "�������� ������ �" << laba << setw(5) << "**" << endl;
	cout << setw(31) << "**" << setw(24) << "���� ������ ����� ����" << setw(4) << "**" << endl;
	cout << setw(30);

	for (int i = 0; i < 30; i++) {
		cout << "*";
	}

	cout << endl;
}

void sort_1(vector<book_lib>& lib){
	if(!lib.empty() && lib.size()>1){
		book_lib buf;
		for(int i=0;i<lib.size();i++){
			for(int j=0;j<lib.size()-1;j++){
				mystr bs = lib[j].FIO;
				mystr bss = lib[j+1].FIO;
				mystr s = strtok(bs,"_");
				mystr ss = strtok(bss,"_");
				if(strcmp(s,ss)>0){
					buf = lib[j];
					lib[j] = lib[j+1];
					lib[j+1] = buf;
				}
			}
		}
	}
}

void sort_2(vector<book_lib>& lib){
	if(!lib.empty() && lib.size()>1){
		book_lib buf;
		for(int i=0;i<lib.size();i++){
			for(int j=0;j<lib.size()-1;j++){
				if(lib[j].year<lib[j+1].year){
					buf = lib[j];
					lib[j] = lib[j+1];
					lib[j+1] = buf;
				}
			}
		}
	}
}