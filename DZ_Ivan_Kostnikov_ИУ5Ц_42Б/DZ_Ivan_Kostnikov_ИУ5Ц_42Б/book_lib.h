#pragma once
#include <iostream>
#include <vector>
#include <iomanip>
#include <string.h>
#include "mystr.h"

using namespace std;

class book_lib
{
	mystr FIO;
	mystr Name;
	int year;
	int count;

public:
	friend ofstream& operator<<(ofstream&, book_lib&);
	friend ifstream& operator>>(ifstream&, book_lib&);
	friend ostream& operator<<(ostream&, book_lib&);
	friend istream& operator>>(istream&, book_lib&);

	book_lib& operator=(book_lib&);
	bool operator==(mystr&);

	friend void sort_1(vector<book_lib>& lib);
	friend void sort_2(vector<book_lib>& lib);
	
	book_lib();
	book_lib(const book_lib&);
	book_lib(mystr FIO,mystr Name,int year,int count);
	~book_lib();
};

void read_lib(mystr, vector<book_lib>&);
void write_lib(mystr, vector<book_lib>&);
void laba(int num_laba);