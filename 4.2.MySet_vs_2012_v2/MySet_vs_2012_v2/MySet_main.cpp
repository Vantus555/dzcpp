#include <iostream>
#include <iomanip>
#include <string>
#include "myvector.h"
#include "MYSet.h"

using namespace std;

void laba(double laba) {// ������� �������
	cout << setw(30);

	for (int i = 0; i < 50; i++) {

		cout << "*";

	}
	cout << endl << setw(31) << "**" << setw(34) << "������������ ������ �" << laba << setw(11) << "**" << endl;
	cout << setw(31) << "**" << setw(37) << "��������� ���������� ����" << setw(11) << "**" << endl;
	cout << setw(30);

	for (int i = 0; i < 50; i++) {
		cout << "*";
	}

	cout << endl;
}

int main() {
	setlocale(0,"ru");
	laba(4.2);
	MySet<int> i(1);
	i.add_element(2);
	i.add_element(3);
	i.add_element(4);
	i.add_element(5);
	i.add_element(6);
	MySet<int> g(1);
	g.add_element(2);
	g.add_element(6);
	g.add_element(7);
	g.add_element(8);
	g.add_element(9);
	g.add_element(10);
	g.add_element(11);
	cout<<"��������� g: "<< g << endl;
	cout<<"��������� i: "<< i << endl;
	MySet<int> c=g-i;
	cout<<"��������� c=g-i: "<< c << endl;
	c=g+i;
	cout<<"��������� c=g+i: "<< c << endl;
	c=g*i;
	cout<<"��������� c=g*i: "<< c << endl;
	
	MyVector<char*> b("hello");
	b.add_element("world");
	b.add_element("!");
	b.add_element("������");
	b.add_element("���");
	b.add_element("!");
	cout<<"������ b: "<< b << endl;

	MySet<char*> s("Yes"),s1,s2;
	s.add_element("������!");
	s.add_element("No");
	char str[]="Hello!";
	s.add_element(str);
	s1.add_element("Cat");
	s1.add_element("No");
	s1.add_element("������!");
	/**/cout<<"��������� s: "<<s<<endl;
	cout << "��������� s1: " << s1 << endl;
	s2 = s1 - s;
	cout << "��������� s2=s1-s: " << s2 << endl;
	s2=s-s1;
	cout<<"��������� s2=s-s1: "<<s2<<endl;
	s2=s1+s;
	cout<<"��������� s2=s1+s: "<<s2<<endl;
	s2=s1*s;
	cout<<"��������� s2=s1*s: "<<s2<<endl;
	MySet<char*> s3 = s2;
	cout<<"��������� s3=s2: "<<s3<<endl;
	cout<<"||-----------��������� += -= *= -----------||\n"<<endl;
	cout<<"��������� s1: "<<s1<<endl;
	s2.delete_element(0);
	s2.delete_element(0);
	s2.add_element("hi");
	s2.add_element("color");
	cout<<"��������� s2: "<<s2<<endl;
	s2+=s1;
	cout<<"��������� s2+=s1: "<<s2<<endl;
	s2-=s1;
	cout<<"��������� s2-=s1: "<<s2<<endl;
	s2*=s1;
	cout<<"��������� s2*=s1: "<<s2<<endl;

	cout<<"||-----------��������� == != -----------||\n"<<endl;

	if(s3==s2)
		cout<<"��������� s3=s2\n";
	else
		cout<<"��������� s3!=s2\n";
	if(s3==s1)
		cout<<"��������� s3=s1\n";
	else
		cout<<"��������� s3!=s1\n";
	if(s1==s3)
		cout<<"��������� s1=s3\n";
	else
		cout<<"��������� s1!=s3\n";/**/

	cout << endl;

	system("pause");
	return 0;
}