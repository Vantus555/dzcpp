#include "myvector.h"

template <class type>
void MyVector<type>::resize() {
	if (size == maxsize) {
		if (maxsize % 2 == 0)
			maxsize *= 1.5;
		else maxsize = maxsize * 1.5 + 1;

		type** buf = new type * [maxsize];

		for (int i = 0; i < size; i++) {
			buf[i] = new type;
			buf[i] = this->pdata[i];
		}
		delete[] this->pdata;
		this->pdata = buf;
	}
	if ((size < (maxsize / 2)) && (maxsize > 5)) {
		type** buf = new type * [maxsize / 1.5];
		int j = 0;
		for (int i = 0; i < size; i++) {
			buf[j] = new type;
			buf[j] = this->pdata[i];
			j++;
		}
		maxsize /= 1.5;
		delete[] this->pdata;
		this->pdata = buf;
	}
}

template <class type>
MyVector<type>::MyVector(type el, int maxsz = MAX_SIZE) {
	this->size = 0;
	this->maxsize = maxsz;
	this->pdata = new type * [maxsz];
	if (el) {
		this->pdata[0] = new type;
		*this->pdata[0] = el;
		size += 1;
	}
}

template <class type>
MyVector<type>::MyVector(MyVector<type>& v) {
	int len = v.maxsize;
	this->pdata = new type * [len];
	for (int i = 0; i < v.size; i++) {
		this->pdata[i] = new type;
		this->pdata[i] = v.pdata[i];
	}
	this->size = v.size;
	this->maxsize = v.maxsize;
}

template <class type>
MyVector<type>::~MyVector() {
	delete[] this->pdata;
}

template <class type>
void MyVector<type>::add_element(type el) {

	//int len = strlen(el);
	if (size == maxsize)
		resize();
	this->pdata[size] = new type;
	*this->pdata[size] = el;
	this->size += 1;
}

template <class type>
bool MyVector<type>::delete_element(int i) {
	if (size != 0 && (i >= 0) && (i < this->size)) {
		this->pdata[i] = nullptr;
		sort();
		this->size -= 1;
		resize();
	}
	else return false;
}

template <class type>
void MyVector<type>::sort() {
	for (int i = 0; i < this->size - 1; i++) {
		if (this->pdata[i] == nullptr) {
			for (int j = i + 1; j < size; j++) {
				if (this->pdata[j] != nullptr) {
					this->pdata[i] = this->pdata[j];
					this->pdata[j] = nullptr;
					break;
				}
			}
		}
	}
}

template <class type>
type MyVector<type>::operator[](int i) {
	//if(is_same<type,char*>::value)
	return *this->pdata[i];
}

template <class type>
MyVector<type>& MyVector<type>::operator=(MyVector<type>& v) {
	if (this->pdata != nullptr)
		delete[] this->pdata;
	int len = v.maxsize;
	this->pdata = new type * [len];
	for (int i = 0; i < v.size; i++) {
		this->pdata[i] = new type;
		this->pdata[i] = v.pdata[i];
	}
	this->size = v.size;
	this->maxsize = v.maxsize;

	return v;
}

template <class type>
int MyVector<type>::find(type el) {
	bool b = true;
	for (int i = 0; i < this->size; i++) {
		if (*this->pdata[i] == el) {
			return i;
			b = false;
			break;
		}
	}
	if (b)
		return -1;
}