#ifndef MYVECTOR_H
#define MYVECTOR_H
#include <iostream>
using namespace std;

const int MAX_SIZE = 5;

template <class type>
class MyVector
{
private:
	void resize();
protected:
	int maxsize;
	int size;
	type ** pdata;
public:
	MyVector(type el = NULL, int maxsz = MAX_SIZE);// �� ��� ����������

	MyVector(MyVector& v);
	~MyVector();

	void add_element(type el);
	bool delete_element(int i);
	type operator[](int i);

	void sort();

	void print();
	int Size(){return size;}
	int Maxsize(){return maxsize;}
	int find(type el);
	MyVector& operator=(MyVector& v);
	friend ostream& operator<<(ostream& out, MyVector& v){
		if (v.size != 0) {
			cout << "{ ";
			for (int i = 0; i < v.size; i++) {
					out << v[i];
				if (i < v.size - 1)
					out << ", ";
				else out << " }" << endl;
			}
			return out;
		}
		else {
			out << "{ NULL }" << endl;
			return out;
		}
	}
};

#endif