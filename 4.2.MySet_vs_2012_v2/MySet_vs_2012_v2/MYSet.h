#ifndef MYSET_H
#define MYSET_H
#include <iostream>
#include "myvector.h"
#include "myvector.cpp"

using namespace std;

template <class type>
class MySet : public MyVector<type>
{
public:
	MySet(type el = NULL) : MyVector(el) {};
	MySet(MySet& s1) : MyVector(s1) {};

	friend MySet operator-(MySet& s1, MySet& s2){
		MySet<type> s;
		s = s1;
			for (int i = 0; i < s1.size; i++) {
				bool b = false;
				for (int j = 0; j < s2.size; j++) {
					if (s2[j] == s1[i]) {
						b = true;
						break;
					}
				}
				if (b) {
					while (s.find(s1[i])!=-1) {
						int g = s.find(s1[i]);
						s.delete_element(g);
					}
				}
			
		}
		return s;
	}
	friend MySet operator+(MySet& s1, MySet& s2){
		MySet<type> s;
		s = s1;
		for (int i = 0; i < s2.size; i++) {
			bool b = true;
			for (int j = 0; j < s1.size;j++) {
				if (s1[j] == s2[i]) {
					b = false;
					break;
				}
			}
			if (b)
				s.add_element(s2[i]);
		}
		return s;
	}
	friend MySet operator*(MySet& s1, MySet& s2){
		MySet<type> s = s1;
			for (int i = 0; i < s1.size; i++) {
			bool b = true;
			for (int j = 0; j < s2.size; j++) {
				if (s2[j] == s1[i]) {
					b = false;
					break;
				}
			}
			if (b) {
				while (s.find(s1[i])!=-1) {
					int g = s.find(s1[i]);
					s.delete_element(g);
				}
			}
		}
	return s;
	}/**/

	bool operator==(MySet& ss){
		bool b = false;
		if (this->size == ss.size) {
			for (int i = 0; i < this->size; i++) {
				for (int j = 0; j < ss.size; j++) {
					if (ss.pdata[j] == this->pdata[i]) {
						b = true;
						break;
					}
				}
			}
		}
		return b;
	}
	MySet operator+=(MySet& ss){
		MySet<type> s = *this;
		for (int i = 0; i < ss.size; i++) {
			bool b = true;
			for (int j = 0; j < this->size;j++) {
				if (s[j] == ss[i]) {
					b = false;
					break;
				}
			}
			if (b)
				s.add_element(ss[i]);
		}
		*this = s;
		return *this;
	}
	MySet operator-=(MySet& ss){
		MySet<type> s = *this;
		MySet<type> s2 = *this;
		for (int i = 0; i < this->size; i++) {
			bool b = false;
			for (int j = 0; j < ss.size; j++) {
				if (ss[j] == s2[i]) {
					b = true;
					break;
				}
			}
			if (b) {
				while (s.find(s2[i])!=-1) {
					int g = s.find(s2[i]);
					s.delete_element(g);
				}
			}
		}
		*this = s;
		return *this;
	}
	MySet operator*=(MySet& ss){
		MySet<type> s = *this;
		MySet<type> s2 = *this;
		for (int i = 0; i < this->size; i++) {
			bool b = true;
			for (int j = 0; j < s.size; j++) {
				if (ss[j] == s2[i]) {
					b = false;
					break;
				}
			}
			if (b) {
				while (s.find(s2[i])!=-1) {
					int g = s.find(s2[i]);
					s.delete_element(g);
				}
			}
		}
		*this = s;
		return *this;
	}
	/*bool is_element(char* el);*/
};
#endif;

