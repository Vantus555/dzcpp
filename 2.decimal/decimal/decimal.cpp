#include "decimal.h"
#include <iomanip>
#include <math.h>

static int l,r;

//////////////////////////////////////////////
//	  ������������ ������� � ��������		//
//////////////////////////////////////////////

void decimal::reduce(){
	if((this->numerator<0) && (this->denomination<0)){
		this->numerator *= -1;
		this->denomination *= -1;
	}
	if(this->denomination < 0){
		this->denomination *= -1;
		this->numerator *= -1;
	}
	if( this->numerator % this->denomination == 0){
		this->numerator /= this->denomination;
		this->denomination = 1;
	}
	else{
		int i=2;
		do{
			if((this->numerator%i == 0) && (this->denomination%i == 0)){
				this->numerator/=i;
				this->denomination/=i;
			}
			else{
				if(i==2) i++;
				else i+=2;
			}
		}while(i<=7);
	}
}


decimal::decimal()
{
}


decimal::~decimal()
{
}


istream& operator>>(istream &in, decimal &d){
	char ch = getchar();
	bool m = false;
	if(ch != '\n')
		cin.putback(ch);
	int res = 0;
	do{
		ch = getchar();
		if(ch == '-'){
			m = true;
			continue;
		}
		if(ch >= '0' && ch <= '9'){
			res = res*10 + (int(ch) - '0');
			continue;
		}
		if(ch=='.'){
			if(m)
				d.numerator = -res;
			else d.numerator = res;
			res = 0;
			int count = 0; //������ ����� �������, �� �������
			int null = 0;//������ ����� . ������ �����
			do{
				ch = getchar();
				if(ch >= '0' && ch <= '9'){
					res = res*10 + (int(ch) - '0');
					count++;
					continue;
				}
				if(ch >= '0' && count==0) null++;
				if(ch == ' ' || ch == '\n'){
					if(ch=='\n'){
						int res2 = res;
						while (res2>1)
						{
							res2/=10;
						}
						d.numerator = d.numerator * pow(10,count) + res;
						d.denomination = pow(10,count);
						res=0;
					}
					
				}
				if(ch == '('){
					//a - ��� ����� ����� �������
					//res2 - ����� � ������� ���������� �����
					//k - �� ���-��
					int k=0,res2=0,a=res;
					do{
						ch = getchar();
						if(ch >= '0' && ch <= '9'){
							res2 = res2*10 + (int(ch) - '0');
							k++;//����� � ������� ���������� �����
							continue;
						}
					}while(ch != ')' && ch != '\n' && ch != ' ');
					d.denomination = 0;
					for(int i=0;i<k;i++)
						a *= 10;
					a+=res2;
					for(int i = 0; i < k;i++){
						d.denomination = d.denomination * 10 + 9;
					}
					for(int i = 0; i < count;i++){
						d.denomination = d.denomination * 10;
					}
					d.numerator = d.denomination * d.numerator + (a-res);
					break;
				}
			}while(ch!=' ' && ch!='\n');
			break;
		}
		if(ch=='/'){
			if(m)
				d.numerator = -res;
			else d.numerator = res;
			res = 0;
				bool n = false;
			do{
				ch = getchar();
				if(ch == '-') n = true;
				if(ch >= '0' && ch <= '9'){
					res = res*10 + (int(ch) - '0');
					continue;
				}
				if(n)
					d.denomination = -res;
				else d.denomination = res;
			}while(ch!=' ' && ch!='\n');

			if(d.denomination == 0) d.denomination = 1;

			break;
		}
		if(ch==' ' || ch=='\n'){
			if(m)
				d.numerator = -res;
			else d.numerator = res;
			d.denomination = 1;
			res=0;
			int res3=0;
			int count=0;
			if(ch==' '){
				do{
					ch = getchar();
					if(ch >= '0' && ch <= '9'){
						res = res*10 + (int(ch) - '0');
						count++;
						continue;
					}
				}while(ch!='/');
				do{
					ch = getchar();
					if(ch >= '0' && ch <= '9'){
						res3 = res3*10 + (int(ch) - '0');
						count++;
						continue;
					}
				}while(ch!=' ' && ch!='\n');

				d.numerator = d.numerator * res3 + res;
				d.denomination = res3;
				if(d.denomination == 0) d.denomination = 1;
			break;
			}
			else {
				d.denomination = 1;
			}
		}
	}while(ch!=' ' && ch!='\n');

	d.reduce();

	return in;
}

ostream& operator<<(ostream &out, decimal &d){
	if((d.denomination!=1) && (abs(d.numerator)<d.denomination))
		out << d.numerator << "/" << d.denomination;
	else if((abs(d.numerator)>d.denomination) && (d.denomination!=1)){
		out << d.numerator/d.denomination << " " << abs(d.numerator%d.denomination) << "/" << abs(d.denomination);
	}
	else
		out << d.numerator;

	return out;
}

decimal& decimal::operator=(decimal& d){
	this->numerator = d.numerator;
	this->denomination = d.denomination;
	return d;
}

decimal decimal::operator+(decimal& d){
	decimal res;
	int a;
	if(this->denomination == d.denomination){
		res.numerator = this->numerator + d.numerator;
		res.denomination = d.denomination;
	}
	else{
		a = this->numerator * d.denomination;
		res.numerator = d.numerator * this->denomination;

		res.numerator += a;
		res.denomination = this->denomination * d.denomination;
	}
	res.reduce();
	return res;
}
decimal decimal::operator-(decimal& d){
	return *this + d*(-1);
}
decimal decimal::operator*(decimal& d){
	decimal res;
	res.numerator = this->numerator * d.numerator;
	res.denomination = this->denomination * d.denomination;
	res.reduce();
	return res;
}
decimal decimal::operator/(decimal& d){
	decimal res;
	res.numerator = this->numerator * d.denomination;
	res.denomination = this->denomination * d.numerator;
	res.reduce();
	return res;
}

bool decimal::operator<(decimal& d){
	if(this->denomination == d.denomination){
		return (this->numerator < d.numerator) ? true : false;
	}
	else {
		l = this->numerator * d.denomination;
		r = d.numerator * this->denomination;
		return (l < r) ? true : false;
	}
}

bool decimal::operator>(decimal& d){
	if(this->denomination == d.denomination){
		return (this->numerator > d.numerator) ? true : false;
	}
	else {
		l = this->numerator * d.denomination;
		r = d.numerator * this->denomination;
		return (l > r) ? true : false;
	}
}

bool decimal::operator<=(decimal& d){
	if(this->denomination == d.denomination){
		return (this->numerator <= d.numerator) ? true : false;
	}
	else {
		l = this->numerator * d.denomination;
		r = d.numerator * this->denomination;
		return (l <= r) ? true : false;
	}
}

bool decimal::operator>=(decimal& d){
	if(this->denomination == d.denomination){
		return (this->numerator >= d.numerator) ? true : false;
	}
	else {
		l = this->numerator * d.denomination;
		r = d.numerator * this->denomination;
		return (l >= r) ? true : false;
	}
}

bool decimal::operator==(decimal& d){
	if(this->denomination == d.denomination){
		return (this->numerator == d.numerator) ? true : false;
	}
	else {
		l = this->numerator * d.denomination;
		r = d.numerator * this->denomination;
		return (l == r) ? true : false;
	}
}

bool decimal::operator!=(decimal& d){
	if(this->denomination == d.denomination){
		return (this->numerator != d.numerator) ? true : false;
	}
	else {
		l = this->numerator * d.denomination;
		r = d.numerator * this->denomination;
		return (l != r) ? true : false;
	}
}

//////////////////////////////////////////////
//		������� ���������� �����			//
//////////////////////////////////////////////

decimal::decimal(int a){
	this->numerator=a;
	this->denomination=1;
}

decimal::decimal(double a){
	double c = a,h = 0;
	int k=0,count=0;
	do{
		k=c*pow(10,count);
		h=k/pow(10,count);
		count++;
	}while(h!=c);

	for(int i=0;i<count-1;i++){
		this->numerator*=10;
	}

	this->numerator = k;
	this->denomination = pow(10,count-1);

	this->reduce();
}

decimal::decimal(const decimal& a){
	this->numerator = a.numerator;
	this->denomination = a.denomination;

	this->reduce();
}
///////////////////////////////////////////

decimal& decimal::operator=(int a){
	this->numerator = a;
	this->denomination = 1;
	return *this;
}

decimal& decimal::operator=(double a){
	double c = a,h = 0;
	int k=0,count=0;
	do{
		k=c*pow(10,count);
		h=k/pow(10,count);
		count++;
	}while(h!=c);

	for(int i=0;i<count-1;i++){
		this->numerator*=10;
	}

	this->numerator = k;
	this->denomination = pow(10,count-1);

	this->reduce();

	return *this;
}

decimal decimal::operator-(int a){
	decimal res;
	res = a;
	*this = *this - res;
	return *this;
}

decimal decimal::operator-(double a){
	decimal res;
	res = a;
	*this = *this - res;
	return *this;
}

decimal decimal::operator+(int a){
	decimal res;
	res = a;
	*this = *this + res;
	return *this;
}

decimal decimal::operator+(double a){
	decimal res;
	res = a;
	*this = *this + res;
	return *this;
}

decimal decimal::operator*(int a){
	decimal res;
	res = a;
	*this = *this * res;
	return *this;
}

decimal decimal::operator*(double a){
	decimal res;
	res = a;
	*this = *this * res;
	return *this;
}

decimal decimal::operator/(int a){
	decimal res;
	res = a;
	*this = *this / res;
	return *this;
}

decimal decimal::operator/(double a){
	decimal res;
	res = a;
	*this = *this / res;
	return *this;
}

/////////////////////////////////////////

decimal operator-(int a,decimal& b){
	decimal res;
	res = a;
	b = res - b;
	return b;
}

decimal operator-(double a,decimal& b){
	decimal res;
	res = a;
	b = res - b;
	return b;
}

decimal operator+(int a,decimal& b){
	decimal res;
	res = a;
	b = res + b;
	return b;
}

decimal operator+(double a,decimal& b){
	decimal res;
	res = a;
	b = res + b;
	return b;
}

decimal operator*(int a,decimal& b){
	decimal res;
	res = a;
	b = res * b;
	return b;
}

decimal operator*(double a,decimal& b){
	decimal res;
	res = a;
	b = res * b;
	return b;
}

decimal operator/(int a,decimal& b){
	decimal res;
	res = a;
	b = res / b;
	return b;
}

decimal operator/(double a,decimal& b){
	decimal res;
	res = a;
	b = res / b;
	return b;
}


///////////////////////////////////////////

bool decimal::operator<(int a){
	decimal res;
	res = a;
	return *this < res;
}

bool decimal::operator<(double a){
	decimal res;
	res = a;
	return *this < res;
}

bool decimal::operator>(int a){
	decimal res;
	res = a;
	return *this > res;
}

bool decimal::operator>(double a){
	decimal res;
	res = a;
	return *this > res;
}

bool decimal::operator<=(int a){
	decimal res;
	res = a;
	return *this <= res;
}

bool decimal::operator<=(double a){
	decimal res;
	res = a;
	return *this <= res;
}

bool decimal::operator>=(int a){
	decimal res;
	res = a;
	return *this >= res;
}

bool decimal::operator>=(double a){
	decimal res;
	res = a;
	return *this >= res;
}

bool decimal::operator==(int a){
	decimal res;
	res = a;
	return *this == res;
}

bool decimal::operator==(double a){
	decimal res;
	res = a;
	return *this == res;
}

bool decimal::operator!=(int a){
	decimal res;
	res = a;
	return *this != res;
}

bool decimal::operator!=(double a){
	decimal res;
	res = a;
	return *this != res;
}

////////////////////////////////////////

bool operator<(int a,decimal& b){
	decimal res;
	res = a;
	return res < b;
}

bool operator<(double a,decimal& b){
	decimal res;
	res = a;
	return res < b;
}

bool operator>(int a,decimal& b){
	decimal res;
	res = a;
	return res > b;
}

bool operator>(double a,decimal& b){
	decimal res;
	res = a;
	return res > b;
}

bool operator<=(int a,decimal& b){
	decimal res;
	res = a;
	return b <= res;
}

bool operator<=(double a,decimal& b){
	decimal res;
	res = a;
	return b <= res;
}

bool operator>=(int a,decimal& b){
	decimal res;
	res = a;
	return res >= b;
}

bool operator>=(double a,decimal& b){
	decimal res;
	res = a;
	return res >= b;
}

bool operator==(int a,decimal& b){
	decimal res;
	res = a;
	return res == b;
}

bool operator==(double a,decimal& b){
	decimal res;
	res = a;
	return res == b;
}

bool operator!=(int a,decimal& b){
	decimal res;
	res = a;
	return res != b;
}

bool operator!=(double a,decimal& b){
	decimal res;
	res = a;
	return res != b;
}
/**/

///////////////////////////////////////////

//////////////////////////////////////////////
//		���������������� �������			//
//////////////////////////////////////////////

void laba(int laba) {// ������� �������
	cout << setw(30);

	for (int i = 0; i < 40; i++) {

		cout << "*";

	}
	cout << endl << setw(31) << "**" << setw(29) << "������������ ������ �" << laba << setw(8) << "**" << endl;
	cout << setw(31) << "**" << setw(32) << "����� ��� ������ � �������" << setw(6) << "**" << endl;
	cout << setw(30);

	for (int i = 0; i < 40; i++) {
		cout << "*";
	}

	cout << endl;
}

void compare(decimal& a,decimal& b){
	if(a < b)
		cout << a << " < " << b << endl;
	if(a > b)
		cout << a << " > " << b << endl;
	if(a == b)
		cout << a << " = " << b << endl;
}

void compare(int a,decimal& b){
	if(a < b)
		cout << a << " < " << b << endl;
	if(a > b)
		cout << a << " > " << b << endl;
	if(a == b)
		cout << a << " = " << b << endl;
}

void compare(decimal& a,int b){
	if(a < b)
		cout << a << " < " << b << endl;
	if(a > b)
		cout << a << " > " << b << endl;
	if(a == b)
		cout << a << " = " << b << endl;
}

void compare(double a,decimal& b){
	if(a < b)
		cout << a << " < " << b << endl;
	if(a > b)
		cout << a << " > " << b << endl;
	if(a == b)
		cout << a << " = " << b << endl;
}

void compare(decimal& a,double b){
	if(a < b)
		cout << a << " < " << b << endl;
	if(a > b)
		cout << a << " > " << b << endl;
	if(a == b)
		cout << a << " = " << b << endl;
}