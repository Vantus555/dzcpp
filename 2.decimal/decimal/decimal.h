#pragma once

#include <iostream>

using namespace std;

class decimal
{
	int numerator;
	int denomination;
	void reduce();
public:
	friend istream& operator>>(istream &in, decimal &str);
	friend ostream& operator<<(ostream &out, decimal &str);

	///////////////////////////////////////////

	decimal& operator=(decimal& d);
	decimal operator+(decimal& d);
	decimal operator-(decimal& d);
	decimal operator*(decimal& d);
	decimal operator/(decimal& d);

	///////////////////////////////////////////

	bool operator<(decimal& d);
	bool operator>(decimal& d);
	bool operator<=(decimal& d);
	bool operator>=(decimal& d);
	bool operator==(decimal& d);
	bool operator!=(decimal& d);

	///////////////////////////////////////////

	decimal& operator=(int d);
	decimal& operator=(double d);
	decimal operator-(int d);
	decimal operator-(double d);
	decimal operator+(int d);
	decimal operator+(double d);
	decimal operator*(int d);
	decimal operator*(double d);
	decimal operator/(int d);
	decimal operator/(double d);

	friend decimal operator-(int d,decimal& b);
	friend decimal operator-(double d,decimal& b);
	friend decimal operator+(int d,decimal& b);
	friend decimal operator+(double d,decimal& b);
	friend decimal operator*(int d,decimal& b);
	friend decimal operator*(double d,decimal& b);
	friend decimal operator/(int d,decimal& b);
	friend decimal operator/(double d,decimal& b);

	///////////////////////////////////////////

	bool operator<(int d);
	bool operator<(double d);
	bool operator>(int d);
	bool operator>(double d);
	bool operator<=(int d);
	bool operator<=(double d);
	bool operator>=(int d);
	bool operator>=(double d);
	bool operator==(int d);
	bool operator==(double d);
	bool operator!=(int d);
	bool operator!=(double d);

	friend bool operator<(int d,decimal& b);
	friend bool operator<(double d,decimal& b);
	friend bool operator>(int d,decimal& b);
	friend bool operator>(double d,decimal& b);
	friend bool operator<=(int d,decimal& b);
	friend bool operator<=(double d,decimal& b);
	friend bool operator>=(int d,decimal& b);
	friend bool operator>=(double d,decimal& b);
	friend bool operator==(int d,decimal& b);
	friend bool operator==(double d,decimal& b);
	friend bool operator!=(int d,decimal& b);
	friend bool operator!=(double d,decimal& b);

	///////////////////////////////////////////

	decimal();
	decimal(const decimal& d);
	decimal(const double b);
	decimal(const int i);
	~decimal();
};

	void laba(int num_laba);
	void compare(decimal& a,decimal& b);
	void compare(int a,decimal& b);
	void compare(decimal& a,int b);
	void compare(double a,decimal& b);
	void compare(decimal& a,double b);
